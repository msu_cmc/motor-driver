EESchema Schematic File Version 2  date Вск 28 Окт 2012 22:19:35
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:arduino_mini_pro
LIBS:mc33926_conn
LIBS:interfaces-ext
LIBS:motor-driver-cache
EELAYER 24  0
EELAYER END
$Descr A4 11700 8267
Sheet 1 1
Title ""
Date "28 oct 2012"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5250 2250 5300 2250
Wire Wire Line
	5300 2250 5300 2950
Wire Wire Line
	5300 2950 2750 2950
Wire Wire Line
	2750 3600 2900 3600
Wire Wire Line
	5250 2650 6850 2650
Wire Wire Line
	2850 2250 2700 2250
Connection ~ 2900 4800
Wire Wire Line
	2450 5550 2900 5550
Wire Wire Line
	2900 5550 2900 4800
Wire Wire Line
	2700 1650 2850 1650
Wire Wire Line
	6350 2050 6350 2450
Wire Wire Line
	6350 2050 5250 2050
Wire Wire Line
	2750 3250 2850 3250
Wire Wire Line
	6850 2150 6850 2350
Wire Wire Line
	7650 2750 7650 3400
Wire Wire Line
	5350 2150 5350 3050
Wire Wire Line
	5350 2150 5250 2150
Wire Wire Line
	2850 2350 2700 2350
Wire Wire Line
	2200 5200 2200 5300
Wire Wire Line
	2200 4300 2200 4400
Wire Wire Line
	3050 900  5450 900 
Wire Wire Line
	3050 900  3050 1550
Wire Wire Line
	3050 1550 2700 1550
Wire Wire Line
	5250 1750 5450 1750
Wire Wire Line
	5250 1650 5300 1650
Wire Wire Line
	5300 1650 5300 1000
Wire Wire Line
	5300 1000 3150 1000
Wire Wire Line
	3150 1000 3150 1750
Wire Wire Line
	3150 1750 2700 1750
Wire Wire Line
	3650 1650 3400 1650
Wire Wire Line
	3400 1650 3400 2050
Wire Wire Line
	3400 2050 2700 2050
Wire Wire Line
	2700 2150 3300 2150
Wire Wire Line
	3300 2150 3300 1550
Wire Wire Line
	3300 1550 3650 1550
Wire Wire Line
	2700 1950 3200 1950
Wire Wire Line
	3200 1950 3200 1100
Wire Wire Line
	3200 1100 5250 1100
Wire Wire Line
	5250 1100 5250 1550
Wire Wire Line
	1950 5550 1800 5550
Connection ~ 1800 5550
Wire Wire Line
	2750 3150 3550 3150
Wire Wire Line
	5350 3050 2750 3050
Wire Wire Line
	1800 5600 1800 5550
Wire Wire Line
	1800 5550 1800 4900
Wire Wire Line
	5450 1750 5450 900 
Wire Wire Line
	1800 6100 1800 6200
Wire Wire Line
	3650 2250 3550 2250
Wire Wire Line
	3550 2250 3550 3150
Wire Wire Line
	4150 3200 4150 3400
Wire Wire Line
	6850 2950 6850 2750
Wire Wire Line
	2750 3350 2850 3350
Wire Wire Line
	6350 2450 6850 2450
Wire Wire Line
	6850 2350 5250 2350
Wire Wire Line
	4150 3400 7650 3400
Wire Wire Line
	1800 4700 1500 4700
Wire Wire Line
	2800 4800 2900 4800
Wire Wire Line
	2900 4800 3000 4800
Wire Wire Line
	3650 2350 3350 2350
Wire Wire Line
	5250 2550 6850 2550
Wire Wire Line
	3450 1950 3650 1950
Wire Wire Line
	2900 3800 2750 3800
$Comp
L CONN_5 P1
U 1 1 508D718C
P 2350 3150
F 0 "P1" V 2300 3150 50  0000 C CNN
F 1 "CONN_5" V 2400 3150 50  0000 C CNN
	1    2350 3150
	-1   0    0    1   
$EndComp
Text Label 2900 3800 0    60   ~ 0
VCC
Text Label 2900 3600 0    60   ~ 0
GND
$Comp
L CONN_2 P3
U 1 1 508D6FDE
P 2400 3700
F 0 "P3" V 2350 3700 40  0000 C CNN
F 1 "CONN_2" V 2450 3700 40  0000 C CNN
	1    2400 3700
	-1   0    0    1   
$EndComp
Text Label 3450 1950 0    60   ~ 0
FBx10
NoConn ~ 3650 2050
NoConn ~ 3650 2150
NoConn ~ 4350 3200
NoConn ~ 4250 3200
Text Label 2850 2250 0    60   ~ 0
VCC
Text Label 3000 4800 0    60   ~ 0
FBx10
Text Label 1500 4700 0    60   ~ 0
FB
Text Label 2850 1650 0    60   ~ 0
FB
NoConn ~ 4750 3200
NoConn ~ 4650 3200
NoConn ~ 5050 2000
NoConn ~ 7650 2350
NoConn ~ 5250 2450
NoConn ~ 7650 2650
NoConn ~ 7650 2550
NoConn ~ 7650 2450
NoConn ~ 4550 3200
NoConn ~ 1750 450 
$Comp
L ARDUINO_MINI_PRO_(PARTIALY) J2
U 1 1 508A9FC0
P 4450 1300
F 0 "J2" H 4450 1300 60  0000 C CNN
F 1 "ARDUINO_MINI_PRO_(PARTIALY)" V 4450 450 60  0000 C CNN
	1    4450 1300
	1    0    0    -1  
$EndComp
$Comp
L MC33926-CONN J1
U 1 1 508A8B45
P 2200 2650
F 0 "J1" H 2200 2600 60  0000 C CNN
F 1 "MC33926-CONN" V 2300 2000 60  0000 C CNN
	1    2200 2650
	-1   0    0    1   
$EndComp
Text Label 2850 3350 0    60   ~ 0
VCC
Text Label 1800 6200 0    60   ~ 0
GND
Text Label 2200 5300 0    60   ~ 0
GND
Text Label 2850 2350 0    60   ~ 0
GND
Text Label 2850 3250 0    60   ~ 0
GND
Text Label 3350 2350 0    60   ~ 0
VCC
Text Label 2200 4300 0    60   ~ 0
VCC
NoConn ~ 2700 2450
NoConn ~ 2700 1850
NoConn ~ 3650 2650
NoConn ~ 3650 2550
NoConn ~ 3650 2450
NoConn ~ 3650 1750
NoConn ~ 3650 1850
Text Label 6850 2950 0    60   ~ 0
VCC
Text Label 6850 2150 0    60   ~ 0
GND
$Comp
L LM358 U1
U 1 1 508A827A
P 2300 4800
F 0 "U1" H 2250 5000 60  0000 L CNN
F 1 "LM358" H 2250 4550 60  0000 L CNN
	1    2300 4800
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 508A829E
P 1800 5850
F 0 "R1" V 1880 5850 50  0000 C CNN
F 1 "220" V 1800 5850 50  0000 C CNN
	1    1800 5850
	-1   0    0    1   
$EndComp
$Comp
L R R2
U 1 1 508A8290
P 2200 5550
F 0 "R2" V 2280 5550 50  0000 C CNN
F 1 "2K" V 2200 5550 50  0000 C CNN
	1    2200 5550
	0    -1   -1   0   
$EndComp
$Comp
L CONN_5X2 P2
U 1 1 508A7EC3
P 7250 2550
F 0 "P2" H 7250 2850 60  0000 C CNN
F 1 "CONN_5X2" V 7250 2550 50  0000 C CNN
	1    7250 2550
	1    0    0    -1  
$EndComp
$EndSCHEMATC
