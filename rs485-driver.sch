EESchema Schematic File Version 2  date Птн 01 Мар 2013 18:25:56
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:interfaces-ext
LIBS:rs485-driver-cache
EELAYER 24  0
EELAYER END
$Descr A4 11700 8267
Sheet 1 1
Title "rs485 driver"
Date "1 mar 2013"
Rev ""
Comp "MSU, VMK, NDSiPU"
Comment1 ""
Comment2 "Goncharov Oleg"
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	4550 2600 4550 2200
Wire Wire Line
	3050 3050 4350 3050
Connection ~ 5050 2300
Connection ~ 4550 2200
Wire Wire Line
	5050 1200 2700 1200
Wire Wire Line
	4350 2200 5350 2200
Wire Wire Line
	3250 2100 2400 2100
Connection ~ 3050 2200
Wire Wire Line
	3050 1950 3050 2200
Wire Wire Line
	3250 2300 2750 2300
Wire Wire Line
	2750 2300 2750 2750
Wire Wire Line
	2750 2750 2400 2750
Wire Wire Line
	2400 1600 2550 1600
Wire Wire Line
	5350 2100 5050 2100
Wire Wire Line
	5050 2100 5050 1200
Wire Wire Line
	5350 2400 5200 2400
Wire Wire Line
	4350 1450 2800 1450
Connection ~ 3050 1450
Wire Wire Line
	4350 1450 4350 2100
Wire Wire Line
	2400 1800 2800 1800
Wire Wire Line
	2800 1800 2800 1450
Wire Wire Line
	2400 1900 2550 1900
Wire Wire Line
	3750 3200 3750 3050
Connection ~ 3750 3050
Wire Wire Line
	5350 2000 5200 2000
Wire Wire Line
	2400 1700 2700 1700
Wire Wire Line
	6300 2400 6150 2400
Wire Wire Line
	2400 2550 2650 2550
Wire Wire Line
	2650 2550 2650 2200
Wire Wire Line
	2650 2200 3250 2200
Wire Wire Line
	3050 2550 3050 2300
Connection ~ 3050 2300
Wire Wire Line
	3250 2400 2900 2400
Wire Wire Line
	2900 2400 2900 2000
Wire Wire Line
	2900 2000 2400 2000
Wire Wire Line
	2700 1700 2700 1200
Wire Wire Line
	4350 2300 5350 2300
Wire Wire Line
	4350 3050 4350 2400
Wire Wire Line
	5050 2600 5050 2300
$Comp
L R R3
U 1 1 5130BA3B
P 4800 2600
F 0 "R3" V 4880 2600 50  0000 C CNN
F 1 "120" V 4800 2600 50  0000 C CNN
	1    4800 2600
	0    1    1    0   
$EndComp
Text Label 2550 2000 0    60   ~ 0
TX
Text Label 2550 2100 0    60   ~ 0
RX
NoConn ~ 6150 2300
NoConn ~ 6150 2200
NoConn ~ 6150 2100
NoConn ~ 6150 2000
Text Label 6300 2400 0    60   ~ 0
RST
Text Label 2550 1600 0    60   ~ 0
RST
$Comp
L CONN_6 P1
U 1 1 50D5F291
P 2050 1850
F 0 "P1" V 2000 1850 60  0000 C CNN
F 1 "CONN_6" V 2100 1850 60  0000 C CNN
	1    2050 1850
	-1   0    0    1   
$EndComp
Text Label 2550 1700 0    60   ~ 0
T0
Text Label 5200 2400 0    60   ~ 0
VCC
Text Label 5200 2000 0    60   ~ 0
GND
Text Label 3750 3200 0    60   ~ 0
GND
Text Label 2550 1800 0    60   ~ 0
VCC
Text Label 2550 1900 0    60   ~ 0
GND
$Comp
L R R1
U 1 1 50D5EE03
P 3050 1700
F 0 "R1" V 3130 1700 50  0000 C CNN
F 1 "10K" V 3050 1700 50  0000 C CNN
	1    3050 1700
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 50D5EDF8
P 3050 2800
F 0 "R2" V 3130 2800 50  0000 C CNN
F 1 "10K" V 3050 2800 50  0000 C CNN
	1    3050 2800
	1    0    0    -1  
$EndComp
$Comp
L CONN_5X2 P3
U 1 1 50D5EDD9
P 5750 2200
F 0 "P3" H 5750 2500 60  0000 C CNN
F 1 "CONN_5X2" V 5750 2200 50  0000 C CNN
	1    5750 2200
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 P2
U 1 1 50D5EDBC
P 2050 2650
F 0 "P2" V 2000 2650 40  0000 C CNN
F 1 "CONN_2" V 2100 2650 40  0000 C CNN
	1    2050 2650
	-1   0    0    1   
$EndComp
$Comp
L MAX485 U1
U 1 1 50D5ED91
P 3800 2250
F 0 "U1" H 3800 2500 60  0000 C CNN
F 1 "MAX485" V 3800 2250 60  0000 C CNN
	1    3800 2250
	1    0    0    -1  
$EndComp
$EndSCHEMATC
